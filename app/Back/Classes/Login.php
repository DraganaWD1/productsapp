<?php

require_once 'Connection/Connection.php';

use Connection;

class Login 
{
    public function __construct()
    {
        
    }

    public static function login($email, $password){
        $db = new Connection;

        $user = $db->prepare('SELECT email, password FROM users WHERE email = :email');
        $user->bindParam(':email', $email);
        $user->execute();
        $user = $user->fetchAll(PDO::FETCH_ASSOC);

        if($user && $user[0]['password'] == $password){
            session_start();
            $_SESSION['login'] = true;

            http_response_code(201);
            echo json_encode($user);

            return;
        }
        http_response_code(201);
        echo json_encode(['message' => 'Not valid credentials']);
    }
}