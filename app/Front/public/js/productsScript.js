$(document).ready(function(){
    $('#back').hide();
    getData();

    $(document).on('click', '.view', function(e){
        let id = e.target.dataset.id;
        $('#tableProducts tbody').html('');
        $.get("../Back/viewProduct.php", {id: id}).then(data => {
            console.log(data)
            createData(data)
            $('#back').show();
        })
    });

    $(document).on('click', '.edit', function(e){
        let id = e.target.dataset.id;
        $.get("../Back/viewProduct.php", {id: id}).then(data => {
            data = JSON.parse(data)[0];
            $('#name').val(data.name);
            $('#description').val(data.description);
            $('#quantity').val(data.quantity)
            $('#addNewProduct').append(`<input type='hidden' id='idProduct' value='${id}'>`)
            $('#name').css('color', '#555');
            $('#description').css('color', '#555');
            $('#submit').attr('id', 'submitEdit');
        })
    });

    $(document).on('click', '.delete', function(e){
        let id = e.target.dataset.id;
        $.get("../Back/deleteProduct.php", {id: id}).then(data => {
            $('#tableProducts tbody').html('');
            getData(data);
        })
    });

    $(document).on('click', '#submitEdit', (e) => {
        e.preventDefault();

        let data = {
            id: $('#idProduct').val(),
            name: $('#name').val(),
            description: $('#description').val(),
            quantity: $('#quantity').val()
        }

        $.post("../Back/updateProduct.php", data).then(res => {
            if(JSON.parse(res).success){
                $('#close').click();
                $('#tableProducts tbody').html('');
                getData();
            }
        })
    })


    $(document).on('click', '#back', () => {
        getData();
    })


    function getData(){
        $.get("../Back/products.php").then(data => {
            createData(data)
            $('#back').hide();
        })
    }

    function createData(data){
        let table = ``;
        JSON.parse(data).forEach(product => {
            table += `
                <tr>
                    <td>${product.id}</td>
                    <td>${product.name}</td>
                    <td>${product.description}</td>
                    <td>${product.quantity}</td>
                    <td>
                        <button data-id='${product.id}' class='btn btn-warning view'>VIEW</button>
                        <button data-id='${product.id}' class='btn btn-primary edit'data-toggle="modal" data-target="#myModal" id="addNew">EDIT</button>
                        <button data-id='${product.id}' class='btn btn-danger delete'>DELETE</button>
                    </td>
                </tr>
            `
        })
        $('#tableProducts tbody').append(table);
    }

    $('#submit').on('click', (e) => {
        e.preventDefault();

        if($('#idProduct').val()) return;
        let data = {
            name: $('#name').val(),
            description: $('#description').val(),
            quantity: $('#quantity').val()
        }

        $.post('../Back/addNewProduct.php', data).then(res => {
            $('#close').click();
            $('#tableProducts tbody').html('');
            getData();
        })
    });
})